# argonone-daemon-arch

The original files has been downloaded from argon40.com

```
curl -O http://download.argon40.com/argon1.sh
curl -O http://download.argon40.com/argonone-irconfig.sh
curl -O http://download.argon40.com/ar1config.png
curl -O http://download.argon40.com/ar1unstall.png
```

Then extracted and tested using Manjaro Arm Minimal on Raspberry Pi 4

## Packages
From official repo

```
pacman -S i2c-tools
```

Cloned from AUR and built using makepkg (be sure to watch any error messages and act accordingly)
```
git clone https://aur.archlinux.org/raspi-gpio-git
cd raspi-gpio-git
makepkg -is
```

```
git clone https://aur.archlinux.org/python-rpi-gpio
cd python-rpi-gpio
makepkg -is
```

```
git clone https://aur.archlinux.org/python-smbus2
cd python-smbus2
makepkg -is
```

## Desktop Launcher
If you want to use the desktop launcher you must ensure the $TERMINAL variable is initialised with your preferred terminal.

## Modify config to load interfaces

```
$ cat /boot/config.txt
 ...
dtparam=spi=on
enable_uart=1
dtparam=i2c_arm=on
```

## Hardware
Power Button Mode jumper pin setting
| PIN setting | Mode             | Behaviour |
| ---         | ---              | ---       |
| Pin 1-2     | Default mode 1   | PRESS button to Power ON from shutdown or power outage |
| Pin 2-3     | Always ON mode 2 | Power current will flow direct to the PI. NO need to PRESS button to power ON from power outage |

## Usage
The followining is from the retail package product folder

### Power button functions
| Argon One Pi state | Action | Function |
| ---                | ---    | ---      |
| OFF                | Short press | Turn ON |
| ON                 | Long press >= 3s | Soft Shutdown and Power Cut |
| ON                 | Short press < 3s | Nothing |
| ON                 | Double tap | Reboot |
| ON                 | Long press >= 5s | Forced shutdown |

### Fan Configuration

```
sudo argonone-config
```
Default settings

| CPU Temp | FAN POWER |
| ---      | ---       |
| 55&deg;C | 10%       |
| 60&deg;C | 55%       |
| 65&deg;C | 100%      |

## Disable WiFi and BT
If used as a server you likely want to disable wifi and bluetooth

```
$ cat /boot/config.txt
  ...
[all]
dtoverlay=disable-wifi
dtoverlay=disable-bt
```
